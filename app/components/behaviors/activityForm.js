var activityForm = {
    submit: function() {
        this.validate();
        this.displayFormErrors(this.errorMessages, this.errors);
        // this.animateFormGroups();
        this.sendForm();
    },
    cancel: function() {
        if (typeof this.init === "function") {
            this.init();
        }
        this.closeActivity();
    },
    hasErrors: function() {
        return Object.keys(this.errors).some(function(key) {
            var errorVal = this.errors[key];
            return errorVal === true;
        }, this);
    },
    addError: function(type, message) {
        this.set("errors." + type, true);
        this.set("errorMessages." + type, message);
        this.displayFormErrors(this.errorMessages, this.errors);
    },
    displayFormErrors: function(errorMessages, errors) {
        var formErrorAlert = this.querySelector("form-error-alert");
        var formGroups = this.querySelectorAll("form-group");

        if (formGroups) {
            formGroups = Array.prototype.slice.call(formGroups);
            formGroups.forEach(function(formGroup) {
                formGroup.computeClass(this.errors);
            }, this);
        }

        if (formErrorAlert) {
            formErrorAlert.setErrors(errorMessages, errors);
        }
    },
    animateFormGroups: function() {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        $("form-group .has-error", this).each(function(index) {
            var self = this;
            setTimeout(function() {
                $(self).toggleClass("lf-animated attention");
            }, index * 200);

            $(this).one(animationEnd, function() {
                $(this).toggleClass("lf-animated attention");
            });
        });
    },
    validate: function() {
        console.warn("You'll need to implement your own validate function to use the activityForm behavior at element", this);
    },
    sendForm: function() {
        console.warn("You'll need to implement your own sendForm function to use the activityForm behavior at element", this);
    },
    attached: function() {
        if (typeof this.$.hub === "undefined") {
            console.warn("ActivityForm behavior requires the event hub to be available locally at this.$.hub");
        }
    }
};