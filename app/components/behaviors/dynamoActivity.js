var dynamoActivity = {
    loadActivity: function() {
        console.warn("DynamoActivity behavior will auto-invoke the load activity method. Ensure it is defined on your element prototype.");
    },
    closeActivity: function() {
        this.$.hub.publish("dynamic:close");
    },
    attached: function() {

        if (typeof this.$.hub === "undefined") {
            console.warn("DynamoActivity behavior requires the event hub to be available locally at this.$.hub");
        }
    },
    resetApp: function() {
        this.init && this.init();
    }
};