
    (function() {
        var component; // This component;
        var http; // http-request service component
        var store; // redux store
        var hub; // event-hub component

        function reducer(state, action) {
            switch (action.type) {
                case "CANCEL_FORM":
                    return state;

                case "RESET_FORM":
                    state.errorMessage = "";
                    state.selectedPaymentMethod = state.currentLoan.paymentMethod;
                    return state;

                case "SET_CURRENT_LOAN":
                    state.currentLoan = action.currentLoan
                    return state;

                case "SET_PAYMENT_METHOD":
                    state.selectedPaymentMethod = action.paymentMethod;
                    return state;

                case "SUBMIT_FORM":
                    state.formData.submitCount += 1;
                    state.formData.isSubmitting = true;
                    return state;

                case "SUBMIT_FORM_FAILURE":
                    state.formData.error = true;
                    state.formData.isSubmitting = false;
                    return state;

                case "SUBMIT_FORM_SUCCESS":
                    state.formData.error = false;
                    state.formData.isSubmitting = false;
                    return state

                case "SET_ERROR_MESSAGE":
                    state.errorMessage = action.errorMessage;
                    return state;

                case "INVALIDATE_BANK_INFO":
                    state.currentLoan.bankInfo = [];
                    return state;

                default:
                    return state;
            }
        }

        store = Redux.createStore(
            // Reference to reducer function
            reducer,

            // Initial state
            {
                selectedPaymentMethod: false,
                currentLoan: false,
                formData: {
                    submitCount: 0
                },
                errorMessage: ""
            },

            // Hook for chrome react dev tools extension
            window.devToolsExtension ? window.devToolsExtension() : undefined
        );

        // Polymer prototype definition as usual
        Polymer({
            is: "modify-payment-method",

            properties: {
                // Definitions for radio button group
                radioButtons: {
                    value: function() {
                        return [{
                            label: 'ACH',
                            value: 'ACH'
                        }, {
                            label: 'Check',
                            value: 'Check'
                        }];
                    }
                },

                alertCfg: {
                    type: Object,
                    value: function() {
                        return {
                            type: "danger",
                            closeAction: "HIDE_ERROR_MESSAGE"
                        }
                    }
                },

                // Used by radioButtonGroup children to fire the correct event type from click handlers
                radioButtonAction: {
                    type: String,
                    value: function() {
                        return "SET_PAYMENT_METHOD"
                    }
                },

                alertClickAction: {
                    type: String,
                    value: function() {
                        return "HIDE_ERROR_MESSAGE"
                    }
                },

                // Store a copy of state locally
                state: {
                    value: function() {
                        return store.getState();
                    }
                },

                // Passed in from parent
                currentLoan: {
                    type: Object
                }
            },

            ready: function() {
                var self = component = this;

                // Make http-request available to the store
                http = self.$.http

                // Make event hub available to the store
                hub = self.$.hub

                // Listen for the events we expect from children
                self.listen(self, "SET_PAYMENT_METHOD", "setPaymentMethodFromChildren");
                self.listen(self, "HIDE_ERROR_MESSAGE", "hideErrorMessage");

                // Pull a fresh copy of state after store dispatches
                store.subscribe(function() {
                    var nextState = JSON.parse(JSON.stringify(store.getState()));
                    self.set("state", nextState);
                });
            },

            detached: function() {
                this.resetForm();
            },

            resetApp: function() {
                this.cancelForm();
            },

            observers: [
                // Used to set currentLoan in the store
                "setCurrentLoan(currentLoan)"
            ],

            invalidateBankInfo: function() {
                store.dispatch({
                    type: "INVALIDATE_BANK_INFO"
                });
            },

            hideErrorMessage: function() {
                store.dispatch({
                    type: "SET_ERROR_MESSAGE",
                    errorMessage: ""
                });
            },

            setPaymentMethodFromChildren: function(ev, detail) {
                this.setPaymentMethod(detail.paymentMethod);
            },

            setPaymentMethod: function(paymentMethod) {
                var state = store.getState();
                var noBankErrorMessage = "No bank account information available.";
                var self = this;

                // Display an error when selectedPaymentMethod is ACH,
                // there is no bank info, and
                // error message is not already set
                if (
                    state.currentLoan &&
                    state.currentLoan.bankInfo.length === 0 &&
                    paymentMethod === 'ACH'
                ) {
                    self.setErrorMessage("No bank account information available.");
                    return;
                }

                store.dispatch({
                    type: "SET_PAYMENT_METHOD",
                    paymentMethod: paymentMethod
                });
            },

            setErrorMessage: function(errorMessage) {
                store.dispatch({
                    type: "SET_ERROR_MESSAGE",
                    errorMessage: errorMessage
                });
            },

            submitForm: function() {
                var self = this;
                store.dispatch({
                    type: "SUBMIT_FORM"
                });
                var state = store.getState();
                var paymentMethod = state.selectedPaymentMethod;
                var referenceNumber = state.currentLoan.referenceNumber;
                var url = "/" + referenceNumber + "/Change/" + paymentMethod;

                this.$.http.post(
                    url, {},
                    window.config.services.loan,
                    function success(response) {
                        // Update submitting status
                        store.dispatch({
                            type: "SUBMIT_FORM_SUCCESS"
                        });

                        // Empty error message
                        store.dispatch({
                            type: "SET_ERROR_MESSAGE",
                            errorMessage: ""
                        });

                        self.$.hub.publish("notification:show", {
                            type: "success",
                            message: "Payment method changed to " + self.state.selectedPaymentMethod,
                        });

                        self.$.hub.publish("loan:refresh");

                        // Close Activity
                        self.$.hub.publish("activity:close", {
                            activity: self.is
                        });
                    },
                    function failure(response) {
                        var message = response.responseJSON.message ||
                            "Unable to process your request. Please contact customer service.";

                        // Update submitting status
                        store.dispatch({
                            type: "SUBMIT_FORM_FAILURE",
                        });

                        // Use error message from response
                        store.dispatch({
                            type: "SET_ERROR_MESSAGE",
                            errorMessage: message,
                            source: "Submit form failure"
                        });
                    }
                );
            },

            cancelForm: function() {
                hub.publish("activity:close", {
                    activity: component.is
                });
                this.resetForm();
                store.dispatch({
                    type: "CANCEL_FORM"
                });
            },

            resetForm: function() {
                store.dispatch({
                    type: "RESET_FORM"
                });
            },

            isCheck: function(paymentMethod) {
                return paymentMethod.toLowerCase() === "check";
            },

            shouldHideBankList: function() {
                var state = store.getState();
                var bankInfo = state.currentLoan.bankInfo;
                var paymentMethod = state.currentLoan.paymentMethod;
                return bankInfo.length === 0 ||
                    paymentMethod.toLowerCase() !== "ach";
            },

            setCurrentLoan: function(currentLoan) {
                store.dispatch({
                    type: "SET_CURRENT_LOAN",
                    currentLoan: currentLoan
                });

                this.setPaymentMethod(currentLoan.paymentMethod);
            },

            shouldHideErrorMessage: function(errorMessage) {
                if (errorMessage) {
                    return errorMessage.length === 0;
                }
                return true;
            }
        });
    })();
