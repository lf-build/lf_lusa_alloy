Polymer({
    is: 'lf-radio-button-group',

    properties: {
        buttonDefinitions: {
            type: Array
        },
        groupValue: {
            type: String
        },
        actionType: {
            type: String
        }
    },

    computeClass: function (btnValue, groupValue) {
        var baseClass = "pull-left lf-radio-button";

        if (btnValue.toLowerCase() === groupValue.toLowerCase()) {
            return baseClass + " lf-radio-button-active";
        } else {
            return baseClass;
        }
    },

    handleClick: function (ev) {
        var paymentMethod = ev.model.btn.value;
        this.fire(this.actionType, {
            paymentMethod: paymentMethod,
            source: "Modify payment method radio button group"
        });
    }

});
