window.config = {
    services: {
        configuration: "//192.168.1.9:5008"
    }
};

var fetch = function (request) {
    window.defaultAppToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJteS1hcHAiLCJpYXQiOiIyMDE1LTExLTE0VDAwOjUxOjE2LjI0MTczNTZaIiwiZXhwIjoiMjAxNi0xMS0xNFQwMDo1MDo1Ni43Njc0NzY0WiIsInN1YiI6ImpvaG5zbWl0aCIsInRlbmFudCI6Im15LXRlbmFudCIsInNjb3BlIjpbImFtb3J0aXphdGlvbi9zY2hlZHVsZSIsImxvYW5zL2RvY3VtZW50cy91cGxvYWQiXSwiSXNWYWxpZCI6ZmFsc2V9.F_0aDeBgLpNGBiD5IVVAgEN07swGpnbhg8-yTWFzqX0"
    window.dispatchEvent(new Event("appTokenLoaded"));
}

Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
 
// loading the request script
var src = "";
if (document.location.hostname == "192.168.0.56") {
    src = "//192.168.0.56/v1/request/read?callback=fetch";
} else {
    var prefix = (document.location.hostname.indexOf("uat") == 0 ? "uat." : "");
    src = "//" + prefix + "api.lendfoundry.com/v1/request/read?callback=fetch"
}

var script = document.createElement('script');
script.src = src;
script.async = false;
document.head.appendChild(script);

